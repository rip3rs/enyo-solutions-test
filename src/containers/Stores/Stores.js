import React from 'react';
import classes from "./Stores.module.css";
import StoreBox from './StoreBox/StoreBox';

const stores = props => {
  let title = props.content ? <h1>{props.content.title}</h1> : null;
  let context = props.content ? <p className={classes.context}>{props.content.context}</p> : null;
  let stores = props.content ? props.content.array.map((store, i) => <StoreBox key={i} logo={store.logo} name={store.name} type={store.type} />) : null;
  console.log(stores)

  return (
    <div className={classes.container}>
      {title}
      {context}
      <div className={classes.storesContainer}>{stores}</div>
    </div>);
};

export default stores;