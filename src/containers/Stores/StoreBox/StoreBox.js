import React from 'react';
import classes from "./StoreBox.module.css";

const storeBox = props => (
  <div className={classes.container}>
    <img className={classes.logo} alt={props.name} src={props.logo} />
    <div>
      <p className={classes.type}>{props.type}</p>
      <h3 className={classes.name}>{props.name}</h3>
    </div>
  </div>
);

export default storeBox;