import React from 'react';
import classes from './Slider.module.css';

const slider = props => (
  <div className={classes.container}>
    <img className={classes.img} alt="slider" src="/images/phone-slide.png" />
  </div>
);

export default slider;