import React from 'react';
import classes from "./Title.module.css";

const title = props => <p className={classes.container}>{props.text}</p>

export default title;