import React from 'react';
import classes from "./TopBar.module.css";

const topBar = props => {
  return (
    <div className={classes.container}>
      <img src="images/logo.png" alt="Landr Logo" />
      <button className={classes.button} >DOWNLOAD</button>
    </div>
  );
};

export default topBar;