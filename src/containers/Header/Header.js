import React from 'react';
import classes from "./Header.module.css";
import TopBar from './TopBar/TopBar';
import Title from './Title/Title';
import Slider from './Slider/Slider';

const Header = props => {
  return (
    <div className={classes.container} >
      <TopBar />
      <Title text={props.title} />
      <Slider />
    </div>
  );
}

export default Header;