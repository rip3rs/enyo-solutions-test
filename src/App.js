import React, { Component } from 'react';
import './App.css';
import Axios from 'axios';
import Header from './containers/Header/Header';
import Stores from './containers/Stores/Stores';


class App extends Component {
  state = {
    content: null
  }

  contentHandler(content) {
    this.setState({ content: content });
  }

  componentDidMount() {
    Axios.get('API/api.json')
      .then(res => this.contentHandler(res.data))
      .catch(err => console.log(err)); //should make a toast if I have time
  }

  render() {
    console.log(this.state)

    return (
      <div className="App">
        <Header title={this.state.content ? this.state.content.header.title : null} />
        <Stores content={this.state.content ? this.state.content.stores : null} />
      </div>
    );
  }
}

export default App;
